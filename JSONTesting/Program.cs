﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using Newtonsoft.Json;
using System.IO;

namespace JSONTesting
{
    class Program
    {
        static AppData createUser(int age, string gender, string name, bool grad)
        {
            AppData temp = new AppData();
            JVFullName name1 = new JVFullName();
            name1.NameName = name;

            temp.JVAge = age;
            temp.JVGender = gender;
            temp.JVFullName = name1;
            temp.JVCollegeGrad = grad;

            return temp;
        }
        
        static String printUser(AppData user)
        {
            String toPrint = "";

            toPrint = "Name: " + user.JVFullName.NameName + " age: " + user.JVAge + " gender: " + user.JVGender + " graduate status: " + user.JVCollegeGrad + " party role: " + user.JVPartyRole;

            return toPrint;
        }

        /*
         * extractDatas - Creates a List of AppDatas taken from the given list of records.
         * @param records - the list of records
         * @return created list of AppData objects.
         */
        static List<AppData> extractDatas(List<Record> records)
        {
            List<AppData> tempAppDatas = new List<AppData>();

            for(int i = 0; i < records.Count; i++)
            {
                tempAppDatas.Add(records.ElementAt(i).appData);
            }

            return tempAppDatas;
        }

        static void Main(string[] args)
        {
            /*
             * Deserialize the JSON file data into a RootObject and print out records contained within.
             */
            RootObject root = JsonConvert.DeserializeObject<RootObject>(File.ReadAllText(@"C:\Users\CheyanneMiller\Documents\visual studio 2017\Projects\JSONTesting\JSONTesting\jsonData.json"));

            //the list of AppDatas from the deserialized root object
            List<AppData> extractedDatas = extractDatas(root.Data.records);

            for (int i = 0; i < extractedDatas.Count; i++)
            {
                //write each AppData's information to the console
                Console.WriteLine(printUser(extractedDatas.ElementAt(i)));
            }
            

            /*
             * Serialize the RootObject into JSON format output into a text file.
             */
            File.WriteAllText(@"C:\Users\CheyanneMiller\Documents\visual studio 2017\Projects\JSONTesting\JSONTesting\dataExport.json", JsonConvert.SerializeObject(root));

            Console.ReadLine();




            // Console.WriteLine(root.Data.records.ElementAt(0).appData.JVAge);
            // Console.ReadLine();


            //RootObject root1 = new RootObject();
            //List<AppData> users = new List<AppData>();


            //AppData person1 = new AppData();

            //users.Add(createUser(20, "male", "Bob", true));
            //users.Add(createUser(25, "Female", "Sarah", false));
            //users.Add(createUser(30, "male", "Jesse", true));
            //users.Add(createUser(23, "female", "Erica", true));
            //users.Add(createUser(40, "male", "Jake", false));

            ////Console.WriteLine(printUser(users.ElementAt(1)));
            ////Console.ReadLine();

            //for (int i = 0; i < users.Count; i++)
            //{
            //    Console.WriteLine(printUser(users.ElementAt(i)));
            //}
            //Console.ReadLine();

        }
    }
}
