﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JSONTesting
{
    public class JVEmail
    {
        public object nwId { get; set; }
        public bool Primary { get; set; }
        public string EmailType { get; set; }
        public bool znwLocked { get; set; }
        public object InternalSeq { get; set; }
        public string EmailAddress { get; set; }
    }

    public class JVSalary
    {
        public string CurrencyCode { get; set; }
        public int CurrencyValue { get; set; }
        public int CurrencyDecimals { get; set; }
    }

    public class JVFullName
    {
        public string NameName { get; set; }
        public string FormatUsed { get; set; }
        public string NameKeyNames { get; set; }
        public string NameNameInverted { get; set; }
        public string NameNamesBeforeKeyNames { get; set; }
    }

    public class AppData
    {
        public string nwId { get; set; }
        public int JVAge { get; set; }
        public List<JVEmail> JVEmails { get; set; }
        public string JVGender { get; set; }
        public JVSalary JVSalary { get; set; }
        public bool JVMarried { get; set; }
        public string JVWebsite { get; set; }
        public bool znwLocked { get; set; }
        public JVFullName JVFullName { get; set; }
        public string JVBiography { get; set; }
        public string JVPartyRole { get; set; }
        public string JVPartyType { get; set; }
        public bool JVCollegeGrad { get; set; }
        public DateTime nwCreatedDate { get; set; }
        public DateTime nwLastModifiedDate { get; set; }
        public string nwTenantStripe { get; set; }
        public string nwCreatedByUser { get; set; }
        public string nwLastModifiedByUser { get; set; }
    }

    public class Record
    {
        public string version { get; set; }
        public string nateDisposition { get; set; }
        public AppData appData { get; set; }
    }

    public class PageData
    {
        public int offset { get; set; }
        public int limit { get; set; }
        public int total { get; set; }
    }

    public class Data
    {
        public string nwTable { get; set; }
        public List<Record> records { get; set; }
        public PageData pageData { get; set; }
    }

    public class RootObject
    {
        public List<object> DebugTrace { get; set; }
        public List<object> ErrorMessages { get; set; }
        public List<object> UserInterfaceHints { get; set; }
        public Data Data { get; set; }
    }
}
