﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NW.API.Schoology.DBLogging.Con
{
    public class DBLogGet {
        public class AppData
        {
            public string nwId { get; set; }
            public string CMAppName { get; set; }
            public string CMMessage { get; set; }
            public bool znwLocked { get; set; }
            public DateTime CMDateTime { get; set; }
            public string CMLogLevel { get; set; }
            public string CMUserName { get; set; }
            public string CMMachineName { get; set; }
            public string CMMethodOrigination { get; set; }
            public DateTime nwCreatedDate { get; set; }
            public DateTime nwLastModifiedDate { get; set; }
            public string nwTenantStripe { get; set; }
            public string nwCreatedByUser { get; set; }
            public string nwLastModifiedByUser { get; set; }
        }

        public class Record
        {
            public string version { get; set; }
            public string nateDisposition { get; set; }
            public AppData appData { get; set; }
        }

        public class PageData
        {
            public int offset { get; set; }
            public int limit { get; set; }
            public int total { get; set; }
        }

        public class Data
        {
            public string nwTable { get; set; }
            public List<Record> records { get; set; }
            public PageData pageData { get; set; }
        }

        public class RootObject
        {
            public List<object> DebugTrace { get; set; }
            public List<object> ErrorMessages { get; set; }
            public List<object> UserInterfaceHints { get; set; }
            public Data Data { get; set; }
        }
    }
    

    public class DBLogPost
    {
        public class ZnwWorkflowInstance
        {
        }

        public class AppData
        {
            public object nwId { get; set; }
            public string CMAppName { get; set; }
            public string CMLogLevel { get; set; }
            public DateTime CMDateTime { get; set; }
            public string CMMethodOrigination { get; set; }
            public string CMUserName { get; set; }
            public object CMMachineName { get; set; }
            public object CMCallSite { get; set; }
            public object CMLogger { get; set; }
            public string CMMessage { get; set; }
            public object nwExternalId { get; set; }
            public object nwCreatedByUser { get; set; }
            public object nwCreatedDate { get; set; }
            public object nwLastModifiedByUser { get; set; }
            public string nwLastModifiedDate { get; set; }
            public object nwTenantStripe { get; set; }
            public object AttachmentGroupId { get; set; }
            public ZnwWorkflowInstance znwWorkflowInstance { get; set; }
            public object znwOwner { get; set; }
            public bool znwLocked { get; set; }
            public object znwSparseOwner { get; set; }
            public object nwNateId { get; set; }
            public object nwNateDisposition { get; set; }
            public object InternalSeq { get; set; }
            public string nwBaseVersion { get; set; }
        }

        public class Record
        {
            public AppData appData { get; set; }
            public string version { get; set; }
            public string nateDisposition { get; set; }
        }

        public class ClientState
        {
            public string RootNwId { get; set; }
            public string SaveType { get; set; }
            public string ApplicationName { get; set; }
        }

        public class RootObject
        {
            public List<Record> records { get; set; }
            public ClientState clientState { get; set; }
            public bool createContainer { get; set; }
            public bool commitContainer { get; set; }
        }
    }

    public class DBLogDelete
    {
        public class AppData
        {
            public object nwBaseVersion { get; set; }
            public string nwId { get; set; }
        }

        public class Record
        {
            public string version { get; set; }
            public string nateDisposition { get; set; }
            public AppData appData { get; set; }
        }

        public class Data
        {
            public string nwTable { get; set; }
            public List<Record> records { get; set; }
        }

        public class RootObject
        {
            public List<object> DebugTrace { get; set; }
            public List<object> ErrorMessages { get; set; }
            public List<object> UserInterfaceHints { get; set; }
            public Data Data { get; set; }
        }
    }
}
