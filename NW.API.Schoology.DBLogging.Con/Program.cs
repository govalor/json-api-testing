﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using RestSharp;
using RestSharp.Authenticators;
using NLog;


namespace NW.API.Schoology.DBLogging.Con
{
    class Program
    {
        static Random ran = new Random();
        private static Logger logger = LogManager.GetCurrentClassLogger();

        static DBLogGet.RootObject getRequest()
        {
            //var client = new RestClient("https://master-api1.nextworld.net/v2/CMDBLogTable?nwPaging={%22limit%22:15,%22offset%22:0}"); //only got first page of records
            var client = new RestClient("https://master-api1.nextworld.net/v2/CMDBLogTable?");
            client.Authenticator = new HttpBasicAuthenticator("cheyanne.miller@nextworld.net", "Pion33rs!");

            var request = new RestRequest("", Method.GET);

            // easy async support
            //client.ExecuteAsync(request, response =>
            //{
            //    Console.WriteLine(response.Content);
            //});

            //non-async to deserialize into an object
            IRestResponse response = client.Execute(request);
            var content = response.Content;

            DBLogGet.RootObject root = JsonConvert.DeserializeObject<DBLogGet.RootObject>(content);

            //test to see if deserialization worked
            Console.WriteLine(root.Data.records.ElementAt(0).appData.CMAppName);

            return root;
        }

        static void postRequest(DBLogPost.RootObject root)
        {
            var client = new RestClient("https://master-api1.nextworld.net/v2/CMDBLogTable");
            client.Authenticator = new HttpBasicAuthenticator("cheyanne.miller@nextworld.net", "Pion33rs!");

            var request = new RestRequest(Method.POST);
            request.AddParameter("application/json", JsonConvert.SerializeObject(root), ParameterType.RequestBody); // adds to POST or URL querystring based on Method

            // easy async support
            client.ExecuteAsync(request, response =>
            {
                Console.WriteLine(response.Content);
            });
        }

        static DBLogDelete.Record getNWID(DBLogGet.RootObject root, int delIndex)
        {
            DBLogDelete.Record tempRec = new DBLogDelete.Record();
            DBLogDelete.AppData tempDel = new DBLogDelete.AppData();

            tempDel.nwId = root.Data.records.ElementAt(delIndex).appData.nwId.ToString();
            tempRec.appData = tempDel;

            return tempRec;
        }

        static string makeURL(DBLogDelete.AppData deleteItem)
        {
            string url = "https://master-api1.nextworld.net/v2/CMDBLogTable/" + deleteItem.nwId;

            return url;
        }

        static void deleteRequest(DBLogGet.RootObject getRoot, int deleteIndex)
        {
            //set up for delete request
            DBLogDelete.Record record = new DBLogDelete.Record();
            DBLogDelete.RootObject deleteRoot = new DBLogDelete.RootObject();

            //create the item to be deleted
            record = getNWID(getRoot, deleteIndex);

            logger.Info("Delete request retrieved nwID for record to be deleted.");

            //set up client for DELETE API call
            string deleteURL = makeURL(record.appData);
            var client = new RestClient(deleteURL);
            client.Authenticator = new HttpBasicAuthenticator("cheyanne.miller@nextworld.net", "Pion33rs!");

            var request = new RestRequest(Method.DELETE);
           
            // easy async support
            client.ExecuteAsync(request, response =>
            {
                Console.WriteLine(response.Content);
            });

        }

        //static void printRecord(List<Record> logList)
        //{
        //    AppData logRecord = new AppData();
        //    string toPrint = "";

        //    for(int i = 0; i < logList.Count; i++)
        //    {
        //        logRecord = logList.ElementAt(i).appData;

        //        toPrint = "App: " + logRecord.CMAppName.ToString() + " Log Level: " + logRecord.CMLogLevel + " Method: " + logRecord.CMMethodOrigination + " User: " + logRecord.CMUserName;
        //        Console.WriteLine(logRecord);
        //    }
        //    Console.ReadLine();
        //}

        static DBLogPost.Record makeRecord(Random r)
        {
            DBLogPost.Record tempRec = new DBLogPost.Record();
            DBLogPost.AppData tempLog = new DBLogPost.AppData();

            tempLog.CMAppName = genApp(r);
            tempLog.CMLogLevel = genLevel(r);
            tempLog.CMMethodOrigination = genMethod(r);
            tempLog.CMUserName = genUser(r);
            tempLog.CMMachineName = genMachine(r);

            tempRec.appData = tempLog;
            return tempRec;
        }

        static string genMachine(Random r)
        {
            int num = r.Next(1, 2000);
            int pc = r.Next(0, 9);
            string s = "";

            if(pc == 9)
            {
                s = num.ToString() + "-pc";
            }
            else
            {
                s = num.ToString();
            }
            return s;
        }

        static string genUser(Random r)
        {
            int num = r.Next(0, 5);
            string s = "";

            switch(num)
            {
                case 0:
                    s = "Mark Ransom";
                    break;
                case 1:
                    s = "Sam Nayrouz";
                    break;
                case 2:
                    s = "Kurt Crockett";
                    break;
                case 3:
                    s = "Cheyanne Miller";
                    break;
                case 4:
                    s = "Jake Vossen";
                    break;
                case 5:
                    s = "Robby Keefer";
                    break;
                default:
                    s = "Kondrad Rogers";
                    break;
            }
            return s;
        }

        static string genMethod(Random r)
        {
            int num = r.Next(0, 5);
            string s = "";

            switch (num)
            {
                case 0:
                    s = "getRequest";
                    break;
                case 1:
                    s = "makeRecord";
                    break;
                case 2:
                    s = "deleteRequest";
                    break;
                case 3:
                    s = "makeURL";
                    break;
                case 4:
                    s = "postRequest";
                    break;
                case 5:
                    s = "getNWID";
                    break;
                default:
                    s = "getRequest";
                    break;
            }
            return s;
        }

        static string genApp(Random r)
        {
            int num = r.Next(0, 8);
            string s = "";

            switch(num)
            {
                case 0:
                    s = "Active Directory";
                    break;
                case 1:
                    s = "CSV Files";
                    break;
                case 2:
                    s = "Discovery";
                    break;
                case 3:
                    s = "Genetec";
                    break;
                case 4:
                    s = "Google Calendar";
                    break;
                case 5:
                    s = "Gmail";
                    break;
                case 6:
                    s = "Magnus Health";
                    break;
                case 7:
                    s = "SchoolAdmin";
                    break;
                case 8:
                    s = "Self Service Password Reset";
                    break;
                default:
                    s = "GetParentStudentDirectory";
                    break;
            }
            return s;
        }

        static string genLevel(Random r)
        {
            int num = r.Next(0, 5);
            string s = "";

            switch(num)
            {
                case 0:
                    s = "Trace";
                    break;
                case 1:
                    s = "Debug";
                    break;
                case 2:
                    s = "Info";
                    break;
                case 3:
                    s = "Warn";
                    break;
                case 4:
                    s = "Error";
                    break;
                case 5:
                    s = "Fatal";
                    break;
                default:
                    s = "Other";
                    break;
            }
            return s;
        }

        static void Main(string[] args)
        {
            //creation of root objects and record list to be used for get/post/delete requests
            DBLogPost.RootObject postRoot = JsonConvert.DeserializeObject<DBLogPost.RootObject>(File.ReadAllText(@"C:\Users\CheyanneMiller\Documents\Visual Studio 2017\Projects\JSONTesting\NW.API.Schoology.DBLogging.Con\DBLogPostRequest.json"));
            DBLogGet.RootObject getRoot = new DBLogGet.RootObject();
            List<DBLogPost.Record> recordsList = new List<DBLogPost.Record>();

            //configuration for nlog
            var config = new NLog.Config.LoggingConfiguration();
            var logfile = new NLog.Targets.FileTarget("logfile") { FileName = "file.txt" };
            var logconsole = new NLog.Targets.ConsoleTarget("logconsole");

            config.AddRule(LogLevel.Info, LogLevel.Fatal, logconsole);
            config.AddRule(LogLevel.Debug, LogLevel.Fatal, logfile);

            NLog.LogManager.Configuration = config;

            logger.Info("Program started");

            //API calls

            //get request to initialize RootObject with data from API call
            getRoot = getRequest();

            //making DB logs to send in post request
            for (int i = 0; i < 10; i++)
            {
                recordsList.Add(makeRecord(ran));
            }
            postRoot.records = recordsList;

            // post request
            postRequest(postRoot);

            ////delete request for i number of records
            //for (int i = 0; i < 5; i++)
            //{
            //    deleteRequest(getRoot, i);
            //}

            logger.Info("Program ended.");

            Console.ReadLine();

            NLog.LogManager.Shutdown();


        }
    }
}
