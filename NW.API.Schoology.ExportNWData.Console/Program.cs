﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace NW.API.Schoology.ExportNWData.Console
{
    class Program
    {
        static async void Main(string[] args) //potentially delete string[] args
        {
            //New HttpClient object
            HttpClient client = new HttpClient();

            //call asychronous network methods
            try
            {
                HttpResponseMessage response = await client.GetAsync("https://apps.nextworld.net/master/#./cmgetparentstudentdirectorycmgetparentstudentdirectory1528901845785/list");
                response.EnsureSuccessStatusCode();

                string respContent = await response.Content.ReadAsStringAsync();

                Console.WriteLine(respContent);
            }
            catch(HttpRequestException e)
            {
                Console.WriteLine("\nException");
                Console.WriteLine("Message:{0} ", e.Message);
            }

            //Dispose of the client
            client.Dispose(true);
        }


    }
}
