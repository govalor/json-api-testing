﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace NW.API.Schoology.NWExportedJSON.Con
{
   /* class Program
    {
        static async void Main() //deleted string[] args
        {
            //New HttpClient object
            HttpClient client = new HttpClient();

            //make url a string
            Uri newURL = new Uri(@"https://apps.nextworld.net/master/#./cmgetparentstudentdirectorycmgetparentstudentdirectory1528901845785/list");
            string stringURL = newURL.ToString();

            //call asychronous network methods
            try
            {
                HttpResponseMessage response = await client.GetAsync(stringURL);
                response.EnsureSuccessStatusCode();

                string respContent = await response.Content.ReadAsStringAsync();

                Console.WriteLine(respContent);
                Console.ReadLine();
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException");
                Console.WriteLine("Message:{0} ", e.Message);
            }

            //Dispose of the client
            client.Dispose();
        }
    }*/

    class Program
    {
        var client = new RestClient("https://master-api1.nextworld.net/v2/CMGetParentStudentDirectory?nwPaging={%22limit%22:15,%22offset%22:0}");
        client.Authenticator = new HttpBasicAuthenticator(cheyanne.miller@nextworld.net, "Pion33rs!");

        var request = new RestRequest("", Method.Get);
       // request.AddParameter("name", "value"); // adds to POST or URL querystring based on Method
        //request.AddUrlSegment("id", "123"); // replaces matching token in request.Resource
    
        // easily add HTTP Headers
        //request.AddHeader("header", "value");

        // add files to upload (works with compatible verbs)
        //request.AddFile(path);

        // execute the request
        IRestResponse response = client.Execute(request);
        var content = response.Content; // raw content as string

        // or automatically deserialize result
        // return content type is sniffed but can be explicitly set via RestClient.AddHandler();
        RestResponse<Person> response2 = client.Execute<Person>(request);
        var name = response2.Data.Name;

        // easy async support
        client.ExecuteAsync(request, response => {
    Console.WriteLine(response.Content);
});

// async with deserialization
var asyncHandle = client.ExecuteAsync<Person>(request, response => {
    Console.WriteLine(response.Data.Name);
});

    // abort the request on demand
    asyncHandle.Abort();
    }
}
