﻿using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RandomNameGeneratorLibrary;

namespace NW.API.Schoology.ExportedData.Con
{
    class Program
    {
        static Random randomGenerator = new Random();

        static void getRequest()
        {
            var client = new RestClient("https://master-api1.nextworld.net/v2/CMGetParentStudentDirectory");
            client.Authenticator = new HttpBasicAuthenticator("cheyanne.miller@nextworld.net", "Pion33rs!");

            var request = new RestRequest(Method.GET);

            // easy async support
            client.ExecuteAsync(request, response => {
                Console.WriteLine(response.Content);
            });
        }

        static void postRequest(RootObject root)
        {
            var client = new RestClient("https://master-api1.nextworld.net/v2/CMGetParentStudentDirectory");
            client.Authenticator = new HttpBasicAuthenticator("cheyanne.miller@nextworld.net", "Pion33rs!");

            var request = new RestRequest(Method.POST);
            request.AddParameter("application/json", JsonConvert.SerializeObject(root), ParameterType.RequestBody);

            // easy async support
            client.ExecuteAsync(request, response =>
            {
                Console.WriteLine(response.Content);
            });
        }

        static Record makeStudents(PersonNameGenerator nameGen, PlaceNameGenerator placeGen, Random numGen, Random gradeGenerator)
        {
            Record tempRec = new Record();
            AppData tempStudent = new AppData();

            int p2 = numGen.Next(0, 6);

            //randomly generate the name attributes
            tempStudent.CMFirstName = nameGen.GenerateRandomFirstName();
            tempStudent.CMMiddleName = " ";
            tempStudent.CMLastName = nameGen.GenerateRandomLastName();

            if (p2 == 6)
            {
                tempStudent.CMParents2 = nameGen.GenerateRandomMaleFirstAndLastName();
                tempStudent.CMParents1 = nameGen.GenerateRandomFemaleFirstName() + " " + tempStudent.CMLastName;

                //randomly generate parent1 address and phone number
                tempStudent.CMAddressBlock1 = gradeGenerator.Next(2, 9999).ToString() + " " + placeGen.GenerateRandomPlaceName() + " St";
                tempStudent.CMCityStatePostBlock1 = placeGen.GenerateRandomPlaceName() + ", CO " + numGen.Next(11111, 99999).ToString();
                tempStudent.CMHomePhone1 = gradeGenerator.Next(222, 999).ToString() + "-" + gradeGenerator.Next(222, 999).ToString() + "-" + gradeGenerator.Next(2222, 9999).ToString();

                //randomly generate parent2 address and phone number
                tempStudent.CMAddressBlock2 = gradeGenerator.Next(2, 9999).ToString() + " " + placeGen.GenerateRandomPlaceName() + " St";
                tempStudent.CMCityStatePostBlock2 = placeGen.GenerateRandomPlaceName() + ", CO " + numGen.Next(11111, 99999).ToString();
                tempStudent.CMHomePhone2 = gradeGenerator.Next(222, 999).ToString() + "-" + gradeGenerator.Next(222, 999).ToString() + "-" + gradeGenerator.Next(2222, 9999).ToString();

            }
            else
            {
                //randomly generate parent names
                tempStudent.CMParents1 = nameGen.GenerateRandomFemaleFirstName() + " and " + nameGen.GenerateRandomMaleFirstName() + " " + tempStudent.CMLastName;
               
                //randomly generate parent address and phone number
                tempStudent.CMAddressBlock1 = gradeGenerator.Next(2, 9999).ToString() + " " + placeGen.GenerateRandomPlaceName() + " St";
                tempStudent.CMCityStatePostBlock1 = placeGen.GenerateRandomPlaceName() + ", CO " + numGen.Next(11111, 99999).ToString();
                tempStudent.CMHomePhone1 = gradeGenerator.Next(222, 999).ToString() + "-" + gradeGenerator.Next(222, 999).ToString() + "-" + gradeGenerator.Next(2222, 9999).ToString();

                //fill second parent values so they're not null in data generation
                tempStudent.CMParents2 = " ";
                tempStudent.CMAddressBlock2 = " ";
                tempStudent.CMCityStatePostBlock2 = " ";
                tempStudent.CMHomePhone2 = " ";
            }

            tempStudent.CMSiblings = nameGen.GenerateRandomFirstName() + " " + tempStudent.CMLastName;

            //randomly generate other student attributes
            tempStudent.CMStudentIDNumber = "" + numGen.Next(2008, 2018).ToString() + "S" + numGen.Next(1111, 9999).ToString();
            tempStudent.CMStudentEmail = "" + tempStudent.CMFirstName + "." + tempStudent.CMLastName + "@govalor.com";
            tempStudent.CMDisplayName = tempStudent.CMFirstName + " " + tempStudent.CMLastName;

            //randomly generate grade level and make it a string value
            tempStudent.CMGradeLevel = gradeGenerator.Next(9, 12).ToString() + "th Grade";
            
            tempRec.appData = tempStudent;
            
            return tempRec;

        }

        static void Main(string[] args)
        {
            RootObject root = JsonConvert.DeserializeObject<RootObject>(File.ReadAllText(@"C:\Users\CheyanneMiller\Documents\Visual Studio 2017\Projects\JSONTesting\NW.API.Schoology.ExportedData.Con\postRequest.json"));
            List<Record> studentList = new List<Record>();
            PersonNameGenerator theNameGenerator = new PersonNameGenerator();
            PlaceNameGenerator placeGenerator = new PlaceNameGenerator();
            Random gradeGen = new Random();

            getRequest();

            //making students
            for(int i = 0; i < 10; i++)
            {
                studentList.Add(makeStudents(theNameGenerator, placeGenerator, randomGenerator, gradeGen));
            }
            root.records = studentList;

            postRequest(root);

           Console.ReadLine();
        }
    }
}