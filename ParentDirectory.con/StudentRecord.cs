﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParentDirectory.con
{
    public class AppData
    {
        public string nwId { get; set; }
        public bool znwLocked { get; set; }
        public string CMLastName { get; set; }
        public string CMParents1 { get; set; }
        public string CMParents2 { get; set; }
        public string CMSiblings { get; set; }
        public string CMFirstName { get; set; }
        public int CMStudentID { get; set; }
        public string CMGradeLevel { get; set; }
        public string CMHomePhone1 { get; set; }
        public string CMHomePhone2 { get; set; }
        public string CMMiddleName { get; set; }
        public string CMDisplayName { get; set; }
        public string CMStudentEmail { get; set; }
        public string CMAddressBlock1 { get; set; }
        public string CMAddressBlock2 { get; set; }
        public string CMCityStatePostBlock1 { get; set; }
        public string CMCityStatePostBlock2 { get; set; }
        public DateTime nwCreatedDate { get; set; }
        public DateTime nwLastModifiedDate { get; set; }
        public string nwTenantStripe { get; set; }
        public string nwCreatedByUser { get; set; }
        public string nwLastModifiedByUser { get; set; }
    }

    public class Record
    {
        public string version { get; set; }
        public string nateDisposition { get; set; }
        public AppData appData { get; set; }
    }

    public class PageData
    {
        public int offset { get; set; }
        public int limit { get; set; }
        public int total { get; set; }
    }

    public class Data
    {
        public string nwTable { get; set; }
        public List<Record> records { get; set; }
        public PageData pageData { get; set; }
    }

    public class RootObject
    {
        public List<object> DebugTrace { get; set; }
        public List<object> ErrorMessages { get; set; }
        public List<object> UserInterfaceHints { get; set; }
        public Data Data { get; set; }
    }
}
