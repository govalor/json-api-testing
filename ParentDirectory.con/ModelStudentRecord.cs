﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParentDirectory.con
{
    public partial class GetParentStudentDirectory_Result
    {
        public Nullable<int> StudentID { get; set; }
        public string DisplayName { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MIddleName { get; set; }
        public string GradeLevel { get; set; }
        public string StudentEmail { get; set; }
        public string Siblings { get; set; }
        public string Parents1 { get; set; }
        public string AddressBlock1 { get; set; }
        public string CityStatePostBlock1 { get; set; }
        public string HomePhone1 { get; set; }
        public string Parents2 { get; set; }
        public string AddressBlock2 { get; set; }
        public string CityStatePostBlock2 { get; set; }
        public string HomePhone2 { get; set; }
    }
}
