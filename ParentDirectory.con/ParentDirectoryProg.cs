﻿//using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using System;

namespace ParentDirectory.con
{
    class ParentDirectoryProg
    {

        static void Main(string[] args)
        {
            //Make root
            RootObject root = makeRoot();

            //the list of AppDatas from the deserialized root object
            List<AppData> extractedDatas = extractDatas(root.Data.records);

            //convert list from my Appdatas to his model of app datas
            List<GetParentStudentDirectory_Result> directoryResults = new List<GetParentStudentDirectory_Result>();
            directoryResults = createDirectory(extractedDatas);

            //print out new list of appdatas
            printStudentRecord(directoryResults);
        }

        //creates the root object using JSON conversion
        static RootObject makeRoot()
        {
            RootObject tempRoot = JsonConvert.DeserializeObject<RootObject>(File.ReadAllText(@"C:\Users\CheyanneMiller\Documents\Visual Studio 2017\Projects\JSONTesting\ParentDirectory.con\modelData2.json"));
            return tempRoot;
        }

        static GetParentStudentDirectory_Result createStudent(AppData s1)
        {
            GetParentStudentDirectory_Result tempStudent = new GetParentStudentDirectory_Result();

            //assign the data from the old model to the elements in the correct model
            tempStudent.LastName = s1.CMLastName;
            tempStudent.FirstName = s1.CMFirstName;
            tempStudent.MIddleName = s1.CMMiddleName;
            tempStudent.DisplayName = s1.CMDisplayName;

            tempStudent.StudentEmail = s1.CMStudentEmail;
            tempStudent.StudentID = s1.CMStudentID;
            tempStudent.Siblings = s1.CMSiblings;

            tempStudent.Parents1 = s1.CMParents1;
            tempStudent.Parents2 = s1.CMParents2;
            tempStudent.HomePhone1 = s1.CMHomePhone1;
            tempStudent.HomePhone2 = s1.CMHomePhone2;
            tempStudent.AddressBlock1 = s1.CMAddressBlock1;
            tempStudent.AddressBlock2 = s1.CMAddressBlock2;
            tempStudent.CityStatePostBlock1 = s1.CMCityStatePostBlock1;
            tempStudent.CityStatePostBlock2 = s1.CMCityStatePostBlock2;
            

            return tempStudent;
        }

        static List<GetParentStudentDirectory_Result> createDirectory(List<AppData> dataList)
        {
            List<GetParentStudentDirectory_Result> tempDir = new List<GetParentStudentDirectory_Result>();

            //add each student record to the directory
            for(int i = 0; i < dataList.Count; i++)
            {
                //create a student record from the AppData student information
                tempDir.Add(createStudent(dataList.ElementAt(i)));
            }

            return tempDir;
        }

        /*
        * extractDatas - Creates a List of AppDatas taken from the given list of records.
        * @param records - the list of records
        * @return created list of AppData objects.
        */
        static List<AppData> extractDatas(List<Record> records)
        {
            List<AppData> tempAppDatas = new List<AppData>();

            for (int i = 0; i < records.Count; i++)
            {
                tempAppDatas.Add(records.ElementAt(i).appData);
            }

            return tempAppDatas;
        }

        //Print each student record
        static void printStudentRecord(List<GetParentStudentDirectory_Result> studentDirectory)
        {
            string student = "";
            GetParentStudentDirectory_Result tempStudent = new GetParentStudentDirectory_Result();

            for (int i = 0; i < studentDirectory.Count; i++)
            {
                tempStudent = studentDirectory.ElementAt(i);
                student = "Name: " + tempStudent.FirstName + " " + tempStudent.LastName;
                Console.WriteLine(student);
            }
            Console.ReadLine();
        }
    }
}
